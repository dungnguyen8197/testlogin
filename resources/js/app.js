require('./bootstrap');
window.Vue = require('vue').default;

import Vue from 'vue';

import List from './components/user/Listuser.vue';
import router from './router/index.js';
import store from './store/index.js';
import App from './components/App.vue';
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Paginate from 'vuejs-paginate'

import ApiSer from "./common/api.ser.js";
Vue.component('paginate', Paginate)
Vue.use(BootstrapVue)
ApiSer.init();
Vue.config.devtools = true
Vue.config.productionTip = false
new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
});
