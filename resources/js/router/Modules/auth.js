import login from '../../components/login.vue'
import regis from '../../components/regis.vue'
import page404 from '../../components/404page'
export default [
  {
    path: '/login',
    name: 'login',
    component: login
  },
  {
    path: '/regis',
    name: 'regis',
    component: regis
  },
  {
    path: '/*',
    name: 'page404',
    component: page404
  }
];
