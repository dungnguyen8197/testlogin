import createuser from '../../components/user/Createuser.vue'
import editeuser from '../../components/user/Edituser.vue'
import listeuser from '../../components/user/Listuser.vue'

export default [
  {
    path: '/user',
    name: 'user.index',
    component: listeuser,
  },
  {
    path: '/user/create',
    name: 'user.create',
    component: createuser,
    meta: { requiresAuth: true }
  },
  {
    path: '/user/edit/:id',
    name: 'user.edit',
    component: editeuser,
    meta: { requiresAuth: true }
  }

];