import product from '../../components/product/Listproduct.vue'

export default [
  {
    path: '/product',
    name: 'product.index',
    component: product,
    meta: { requiresAuth: true }
  }

];
