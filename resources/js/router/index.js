import Auth from './Modules/auth.js'
import User from './Modules/user.js'
import Catogory from './Modules/catogory.js'
import Product from './Modules/product.js'
import Vue from "vue";
import Router from "vue-router";
import ApiSer from '../common/api.ser'
import JwtService from '../common/jwt.service'
import store from '../store/index.js'
Vue.use(Router);
const routes = [
  ...User,
  ...Catogory,
  ...Product,
  ...Auth
];
const router = new Router({
  routes,
  mode: 'history',
});
router.beforeEach((to, from, next) => 
{
    if (to.matched.some(record => record.meta.requiresAuth)) 
    {
      ApiSer.setHeader();
      ApiSer.get('api/user')
      .then(response => {
        if (response.status==200) next()
      })
      .catch(error=>{
        if (error.response.status==401){
          alert('token null hoac da bi thay doi');
          next({ name: 'login' })
        } 
      })
    }
    else next()

});

export default router;