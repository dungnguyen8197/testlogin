import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import JwtService from "./jwt.service";
export default {
  init() {
    Vue.use(VueAxios, axios);
    Vue.axios.defaults.baseURL = process.env.BASE_URI;
    /* const TIMEOUT = 1000000;
     const onRequestSuccess = config => {
      const token = localStorage.getItem('jhi-authenticationToken') || sessionStorage.getItem('jhi-authenticationToken');
      if (token) {
        if (!config.headers) {
          config.headers = {};
        }
        config.headers.Authorization = 'Bearer ' + ` ${JwtService.getToken()}`;
      }
      config.timeout = TIMEOUT;
      config.url = `${SERVER_API_URL}${config.url}`;
      return config;
    };
    const setupAxiosInterceptors = onUnauthenticated => {
      const onResponseError = err => {
        const status = err.status || err.response.status;
        if (status === 403 || status === 401) {
          onUnauthenticated();
          route.push('/login');
        }
        return Promise.reject(err);
      };
      if (axios.interceptors) {
        axios.interceptors.request.use(onRequestSuccess);
        axios.interceptors.response.use(res => res, onResponseError);
      }
    };*/

  },
  setHeader() {
    Vue.axios.defaults.headers.common[
      "Authorization"
    ] = 'Bearer ' + ` ${JwtService.getToken()}`;
  },
  post(resource, params) {
    return Vue.axios.post(`${resource}`, params);
  },
  get(resource, params) {
    return Vue.axios.get(`${resource}`);
  },
  update(resource, params) {
    return Vue.axios.put(`${resource}`, params);
  },
  delete(resource) {
    return Vue.axios.delete(resource);
  }
};
