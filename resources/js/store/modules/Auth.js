import { getToken, saveToken } from '../../common/jwt.service'
import { RESOURCE_LOGIN } from '../../api';
const axios = require('axios');

const state = {
  user: {}
};

const actions = {
  login({ }, user) {
    return new Promise((resolve, reject) => {
      axios.post(`${RESOURCE_LOGIN}`, {
        email: user.email,
        password: user.password
      })
        .then((response) => {

          resolve(response);
        })
        .catch((error) => {
          reject(error)

        })
    })
  },
  Regis({ }, user) {
    return new Promise((resolve, reject) => {
      axios.post(`${RESOURCE_USER}`, {
        name: user.name,
        email: user.email,
        password: user.password
      })
        .then((response) => {

          resolve(response);

        })
        .catch((error) => {
          reject(error);

        })
    })

  }
};
export default {
  state,
  actions
};