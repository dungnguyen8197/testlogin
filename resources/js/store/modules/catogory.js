import JwtService from '../../common/jwt.service'
import ApiSer from '../../common/api.ser'
import { RESOURCE_CATO } from '../../api';
const axios = require('axios');

const state = {
  catogories: [],
  catogory: {
    id:'',
    description:'',
    name:''
  },
 
};
const getters= {
  catogory(state){
    return state.catogory;
  }
};
const actions = {
  getlistCat({ }, params) {
    return new Promise((resolve, reject) => {
      ApiSer.get(`${RESOURCE_CATO}${params}`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error)

        })
    })
  },
  addCat({ }, params) {
    return new Promise((resolve, reject) => {
      ApiSer.post(`${RESOURCE_CATO}`, {
        name: params.catogory.name,
        description: params.catogory.description
      })
        .then((response) => {
          resolve(response)
        })
        .catch((error) => {
          reject(error);

        })
    })
  },
  showeditCat({ }, id) {
    return new Promise((resolve, reject) => {
      ApiSer.get(`${RESOURCE_CATO}/${id}/edit`)
        .then(response => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        })
    })
  },
  editCat({ }, params) {
    return new Promise((resolve, reject) => {
      ApiSer.update(`${RESOURCE_CATO}/${params.id}`, {
        name: params.catogory.name,
        description: params.catogory.description

      })
        .then((response) => {

          resolve(response);

        })
        .catch((error) => {
          reject(error);

        })
    })
  },
  deleteCat({ }, id) {
    return new Promise((resolve, reject) => {
      ApiSer.delete(`${RESOURCE_CATO}/${id}`)
        .then((response) => {

          resolve(response);

        })
        .catch((error) => {
          reject(error);

        })
    })
  }
};
export default {
  state,
  getters,
  actions
};