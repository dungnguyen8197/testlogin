import JwtService from '../../common/jwt.service'
import ApiSer from '../../common/api.ser'
import { RESOURCE_USER } from '../../api';
const axios = require('axios');

const state = {
  users: [],
  user: {},
  name: '',
  email: '',
  password: '',
  isClose:false
};
const getters = {
  isLoggedIn: state => {
    return !!JwtService.getToken()
  }
};
const actions = {
  getlistUser({ }, params) {
    return new Promise((resolve, reject) => {
      ApiSer.get(`${RESOURCE_USER}${params}`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error)

        })
    })
  },
  addUser({ }, user) {
    return new Promise((resolve, reject) => {
      ApiSer.post(`${RESOURCE_USER}`, {
        name: user.name,
        email: user.email,
        password: user.password
      })
        .then((response) => {

          resolve(response);

        })
        .catch((error) => {
          reject(error);

        })
    })
  },
  showedit({ }, id) {
    return new Promise((resolve, reject) => {
      ApiSer.get(`${RESOURCE_USER}/${id}/edit`)
        .then((response) => {

          resolve(response);

        })
        .catch((error) => {
          reject(error);

        })
    })
  },
  editUser({ }, params) {
    return new Promise((resolve, reject) => {
      ApiSer.update(`${RESOURCE_USER}/${params.id}`, {
        name: params.user.name,
        email: params.user.email,
        password: params.user.password
      })
        .then((response) => {

          resolve(response);

        })
        .catch((error) => {
          reject(error);

        })
    })
  },

  deleteUser({ }, id) {
    return new Promise((resolve, reject) => {
      ApiSer.delete(`${RESOURCE_USER}/${id}`)
        .then((response) => {

          resolve(response);

        })
        .catch((error) => {
          reject(error);

        })
    })
  }

};
export default {
  state,
  getters,
  actions
};