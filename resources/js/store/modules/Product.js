import JwtService from '../../common/jwt.service'
import ApiSer from '../../common/api.ser'
import { RESOURCE_PRO } from '../../api';
const axios = require('axios');

const state = {
  products: [],
  product: {}
};

const actions = {
  getlistPro({ }, params) {
    return new Promise((resolve, reject) => {
      ApiSer.get(`${RESOURCE_PRO}${params}`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error)

        })
    })
  },
  addPro({ }, data, config) {
    return new Promise((resolve, reject) => {
      ApiSer.post(`${RESOURCE_PRO}`, data, config)
        .then((response) => {

          resolve(response);

        })
        .catch((error) => {
          reject(error);

        })
    })
  },
  showeditPro({ }, id) {
    return new Promise((resolve, reject) => {
      ApiSer.get(`${RESOURCE_PRO}/${id}/edit`)
        .then(response => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        })
    })
  },
  editPro({ }, params) {
    return new Promise((resolve, reject) => {
      ApiSer.post(`${RESOURCE_PRO}/${params.id}`, {
        name: params.product.name,
        catogory: params.product.catogory,
        image: params.product.image,
      }, config)
        .then((response) => {

          resolve(response);

        })
        .catch((error) => {
          reject(error);

        })
    })
  },
  deletePro({ }, id) {
    return new Promise((resolve, reject) => {
      ApiSer.delete(`${RESOURCE_PRO}/${id}`)
        .then((response) => {

          resolve(response);

        })
        .catch((error) => {
          reject(error);

        })
    })
  }
};
export default {
  state,

  actions
};