import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user.js'
import Auth from './modules/Auth.js'
import catogory from './modules/catogory.js'
import product from './modules/Product.js'
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    user,
    Auth,
    catogory,
    product
  }
})
