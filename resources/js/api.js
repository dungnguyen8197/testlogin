const RESOURCE_USER = '/api/users';
const RESOURCE_LOGIN = '/login';
const RESOURCE_CATO = '/api/catogories';
const RESOURCE_PRO = '/api/products';
export {
    RESOURCE_USER, RESOURCE_LOGIN, RESOURCE_CATO, RESOURCE_PRO
}