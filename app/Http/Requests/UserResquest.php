<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserResquest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:11|min:3',
             'email' => 'required|email',
             'password'=>'required',
        ];
    }
    public function messages()
     {
         return [
            'name.required'=>'name chua dien',
            'name.max' => 'Nội dung có độ dài ngắn hơn 11 kí tự',
            'name.min' => 'Nội dung có độ dài dài hơn 3 kí tự',
            'email.required'=>'email trong',
            'email.email'=>'phai co dau @',
            'password.required'=>'password chua dien',
         ];
     }
}
