<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\User\UserEloquentRepository;
use App\Http\Requests\UserResquest;
class UserController extends Controller
{

    protected $userEloquentRepository;
    
   
    public function __construct(UserEloquentRepository $userEloquentRepository)
    {
        $this->UserEloquentRepository = $userEloquentRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = $this->UserEloquentRepository->paginate(4);
        return responseOk($user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserResquest $request)
    {    
        $rule = $request->only(['email', 'name', 'password']);
        $user = $this->UserEloquentRepository->create($rule);
        return responseCreated($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->UserEloquentRepository->find($id);
        if (!$user) {
            return responseNotFound('Error: The user is not exists');
        }
        return responseOk($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserResquest $request, $id)
    {
         
        $rule=array(
            'name'=>$request->name,
            'email' =>  $request->email,
            'password' =>  bcrypt($request->password),
        );
        $user=$this->UserEloquentRepository->update($id,$rule);
        if (!$user) {
            return responseNotFound('Error: The user is not exists');
        }
        return responseUpdated($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $user = $this->UserEloquentRepository->delete($id);
        return responseDeleted();
    }
    
}
