<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Catogory\CatogoryEloquentRepository;
class catogoryController extends Controller
{
    protected $catRepository;
    public function __construct(CatogoryEloquentRepository $catRepository)
    {
        $this->CatogoryEloquentRepository = $catRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //  return responseNotFound('ghg');
        $catogory = $this->CatogoryEloquentRepository->paginate(4);
        return responseOk($catogory);
            
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
       // dd($Requestst);
        $rule=array(
            'name'=>$request->name,
            'description' => $request->description,
        );

       $catogory=$this->CatogoryEloquentRepository->create($rule);
        return responseCreated($catogory);
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $catogory = $this->CatogoryEloquentRepository->find($id);
        if (! $catogory) {
        return responseNotFound('Error: The catogory is not exists');
        }
        return responseOk($catogory);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function update(Request $request, $id)
    {
         
        $rule=array(
            'name'=>$request->name,
            'description' =>  $request->description,
        );
        $catogory = $this->CatogoryEloquentRepository->update($id,$rule);
        if (! $catogory) {
             return responseNotFound('Error: The catogory is not exists');
        }
        return responseUpdated($catogory);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $catogory=$this->CatogoryEloquentRepository->delete($id);
        return responseDeleted();
    }
}
