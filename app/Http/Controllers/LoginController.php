<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Laravel\Passport\HasApiTokens;

class LoginController extends Controller
{       
    public function authenticate(Request $request)
    {   
        $email=$request->email;
        $password=$request->password;

        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            $user=User::where('email',$request->email)->first();
            $user->token=$user->createToken('remember_token')->accessToken;
           // $user->isAuthenticated=window.localStorage.setItem('isAuthenticated',false);
            if (Auth::check()) {
                $user->islogin=true;
            }  
            else{
                $user->islogin=false;
            }
            return response()
            ->json(['user'=>$user,'message'=>'Success:login success'],200);
        }
        
            return response()
            ->json(['message' => 'Fail: login fail'],401);
        
         
    }   
  /*  public function checklogin(){
        if(Auth::check()){
            return response()->json(['isLogin'=>'true'])
        }
        else{
            return response()->json(['isLogin'=>'false'])
        }
    }*/
    
}
