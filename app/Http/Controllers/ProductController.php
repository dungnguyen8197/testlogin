<?php

namespace App\Http\Controllers;
use App\Http\Requests\ProductRequest;
use Illuminate\Http\Request;
use App\Repositories\Product\ProductEloquentRepository;
use App\Models\catogory;
use Str;
class ProductController extends Controller
{
    protected $ProRepository;
    public function __construct(ProductEloquentRepository $ProRepository)
    {
        
        $this->ProductEloquentRepository = $ProRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $product = $this->ProductEloquentRepository->paginate(4);
        $catogory=catogory::all();
        return response()
            ->json(['product'=>$product,'catogory'=>$catogory,'message'=>'success:get success'],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {   
       
        if($request ->hasFile ('image')){
          //  $validated = $request->image;
            $file=$request->file('image');
           
            $rule=array(
                'name'=>$request->name,
                'catogory' => $request->catogory,
                'image' => $this->ProductEloquentRepository->moveimage($file),
            );
        }
        else{
            $rule=array(
                'name'=>$request->name,
                'catogory' =>  $request->catogory,
                'image'=>'',
            );
        }
        $product=$this->ProductEloquentRepository->create($rule);
        return response()
            ->json(['message' => 'Success: You have added an product success'],201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = $this->ProductEloquentRepository->find($id);
        if (! $product) {
        return response()
            ->json(['message' =>'Error: The product is not exists'],400);
        }
        return response()
            ->json(['product'=>$product,'message'=>'Success:get product to edit success'],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, ProductRequest $request)
    {  
       if($request ->hasFile ('image')){
            //$validated = $request->image;
            $file=$request->file('image');
           
            $rule=array(
                'name'=>$request->name,
                'catogory' => $request->catogory,
                'image' => $this->ProductEloquentRepository->moveimage($file),
            );
        }
         else{
            $rule=array(
                'name'=>$request->name,
                'catogory' =>  $request->catogory,
            );
        }   
        $product = $this->ProductEloquentRepository->update($id,$rule);
        return response()
            ->json(['message' => 'Success: You have updated the product'],204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $product=$this->ProductEloquentRepository->delete($id);
        return response()
            ->json(['message' => 'Success: You have deleted the product'],204);
    }
}
