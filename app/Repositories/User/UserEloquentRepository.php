<?php
namespace App\Repositories\User;

use App\Repositories\EloquentRepository;
use Illuminate\Support\Carbon;
use Illuminate\Auth\AuthenticationException;

class UserEloquentRepository extends EloquentRepository
{

    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return \App\Models\User::class;
    }

   /*public function find ($id) {
   		$user = $this->_model->find($id);
   		if(!$user) {
   			throw new NotFoundException('123');
   		}
   }*/
}