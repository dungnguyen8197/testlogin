<?php
namespace App\Repositories\Catogory;

use App\Repositories\EloquentRepository;
use Illuminate\Support\Carbon;
class CatogoryEloquentRepository extends EloquentRepository
{

    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return \App\Models\catogory::class;
    }

}