<?php
namespace App\Repositories\Product;

use App\Repositories\EloquentRepository;
use Illuminate\Support\Carbon;
use Str;
class ProductEloquentRepository extends EloquentRepository
{

    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return \App\Models\Product::class;
    }
    public function moveimage($file){
       // try {
    	$name=$file->getClientOriginalName();
           /* $image=str::random(4)."_".$name;
            while (file_exists("image/".$image)) {
                $image=str::random(4)."_".$name;
            }
             $file->move("image/product",$image);
        return $image;
        */
            $file->move("image/product",$name);
        /*} catch (\Exception $e) {
            return false;
        }*/
        return $name;
    }

}