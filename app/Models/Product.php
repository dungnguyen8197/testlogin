<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';
    protected $fillable = ['id','name','catogory','image','created_at','updated_at'];
    public function Product(){
    	return $this->hasMany('App\Models\Product','id','id');
    }
}
