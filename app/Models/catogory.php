<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class catogory extends Model
{
    protected $table = 'catogory';
    protected $fillable = ['id','name','description','created_at','updated_at'];
    public function catogory(){
    	return $this->belongsTo('App\Models\catogory','id','id');
    }
}
