<?php 

function responseOk ($data) {
	return response()->json([
		'data' => $data
	]);
}
function responseCreated($data) {
	return response()->json([
		'data'=>$data,
		'message' => 'Success:created success'
	], 201);
}

function responseUpdated($data) {
		return response()->json([
		'data' => $data
	], 201);
}

function responseDeleted () {
	return response()->json([
		'message' => 'Success:delete success'
	], 204);
}
function responseNotFound($message) {
		return response()->json([
		'message' => $message
	], 404);
}
